<?php

namespace lenal\collections\Facades;

use Illuminate\Support\Facades\Facade;

class Collections extends Facade {

    public static function getFacadeAccessor()
    {
        return 'collections';
    }
}