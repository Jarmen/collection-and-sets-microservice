<?php

namespace lenal\collections\Facades;

use Illuminate\Support\Facades\Facade;

class Sets extends Facade {

    public static function getFacadeAccessor()
    {
        return 'sets';
    }
}