<?php

namespace lenal\collections;

use Illuminate\Support\ServiceProvider;

class CollectionsServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('collections', 'lenal\collections\Helpers\Collections');
        $this->app->bind('sets', 'lenal\collections\Helpers\Sets');
    }

    public function boot()
    {
        //$this->loadViewsFrom(realpath(dirname(__FILE__)) . '/resources/views', 'collections');
        $this->loadRoutesFrom(realpath(dirname(__FILE__)) . '/routes.php');

        $this->publishes([
            realpath(dirname(__FILE__) . '/config/collections.php') => config_path('collections.php')
        ], 'config');

        $this->publishes([
            base_path('vendor/lenal/collections/') => base_path('packages/lenal/collections')
        ]);
    }
}