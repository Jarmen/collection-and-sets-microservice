<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use lenal\collections\Facades\Collections;
use Illuminate\Support\Str;
use App\SlackApi\SlackApiClient;
use Illuminate\Http\Request;

class JsonCollectionsParserController extends Controller
{
    const EMPTY_FOLDER = 'Скрипт парсера файлов коллекции завершил работу, но никаких изменений не произошло';
    const DUPLICATED_PRODUCT = 'При обработке файла коллекции обнаружены дубликаты товаров.';

    protected $slack_client;

    public function __construct(SlackApiClient $slack_client)
    {
        $this->slack_client = $slack_client;
    }


    public function parse(Request $request)
    {
        $collection =  $request->all();

        $this->storeCollection($collection);
    }


    protected function storeCollection($collection)
    {
        $duplicate_products = null;

        $created_collection = Collections::createCollection([
            'collection_id' => Str::slug($collection['PromoActionName'], '_'),
        ]);

        $created_products_collection = Collections::createProductCollection([
            'active' => 1,
            'name' => $collection['PromoActionName'],
            'slug' => Str::slug($collection['PromoActionName'], '_'),
            'date_from' => $collection['PromoActionStartDate'],
            'date_to' => $collection['PromoActionEndDate']
        ]);

        $this->sendMessage(
            'Создана коллекция ' . $collection['PromoActionName'] . ' с товарами'
            . PHP_EOL
            . 'Дата начала - ' . $collection['PromoActionStartDate']
            . PHP_EOL
            . 'Дата окончания - ' .  $collection['PromoActionEndDate']
        );

        foreach ($collection['Discount'] as $collection_entity) {
            $created_benefit = Collections::createBenefit([
                'collection_id' => $created_collection->id,
                'name' => 'percent',
                'benefit_value' => $collection_entity['DiscountAmount']
            ]);

            $this->sendMessage(
                'Создана сущность Выгода '
                . 'Тип - '
                . $collection_entity['DiscountType']
                . ' Значение - '
                . $collection_entity['DiscountAmount']
            );

            $created_condition = Collections::createCondition([
                'name' => 'amount_in_basket',
                'condition_value' => $collection_entity['Condition'][0]['ConditionRate'],
                'benefit_id' => $created_benefit->id,
                'collection_id' => $created_collection->id
            ]);

            $this->sendMessage(
                'Создана сущность Условие коллекции '
                . 'Тип - количество в корзине '
                . 'Значение - '
                . $collection_entity['Condition'][0]['ConditionRate']
            );
        }

        foreach ($collection['Discount'][0]['DiscountSegmentItemNo'] as $index => $product_barcode) {

            if (Collections::findByBarcode($product_barcode)) {
                $duplcated_products[] = $product_barcode;
                continue;
            }

            Collections::createProduct([
                'collection_id' => $created_collection->id,
                'barcode' => $product_barcode
            ]);
        }

        if (! empty($duplcated_products)) {
            $this->sendMessage(self::DUPLICATED_PRODUCT . ' Артикулы: ' . implode(',', $duplcated_products));
        }
    }

    protected function sendMessage($message)
    {
        $this->slack_client->sendMessage([
            'text' => $message
        ]);
    }
}