<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use lenal\collections\Helpers\RecountOrder;

class OneBoxOrdersController extends Controller
{
    protected $recount;

    public function __construct(RecountOrder $recount)
    {
        $this->recount = $recount;
    }

    public function recount(Request $request)
    {
        $order = $request->all();

        return json_encode($this->recount->recount($order));
    }
}