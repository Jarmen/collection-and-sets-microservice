<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use lenal\collections\Facades\Collections;

class CollectionsController extends Controller
{
    public function index()
    {
        $collections = Collections::getActiveCollections();

        return $collections;
    }

    public function disable()
    {
        $collections = Collections::getAllCollections();

        foreach ($collections as $collection) {
            Collections::checkDateAndDisable($collection);
        }
    }
}