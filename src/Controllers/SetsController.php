<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use lenal\collections\Facades\Sets;

class SetsController extends Controller
{
    public function index()
    {
        $sets = Sets::getActiveSets();

        return $sets;
    }

    public function indexSetThemes()
    {
        $set_themes = Sets::getActiveSetThemes();

        return $set_themes;
    }

    public function disable()
    {
        $sets = Sets::getAllSets();

        foreach ($sets as $set) {
            Sets::checkDateAndDisable($set);
        }
    }
}