<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use lenal\collections\Helpers\RecountCart;

class SiteOrdersController extends Controller
{
    protected $recount;

    public function __construct(RecountCart $recount)
    {
        $this->recount = $recount;
    }
    public function recount(Request $request)
    {
        $cart = $request->get('cart');

        return json_encode($this->recount->recount($cart));
    }
}