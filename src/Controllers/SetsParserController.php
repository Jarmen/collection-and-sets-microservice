<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\File;
use Orchestra\Parser\Xml\Document;
use Orchestra\Parser\Xml\Reader;
use lenal\collections\Facades\Sets;
use Illuminate\Support\Str;
use App\SlackApi\SlackApiClient;

class SetsParserController extends Controller
{
    const EMPTY_FOLDER = 'Скрипт парсера файлов комплектов завершил работу, но никаких изменений не произошло';

    protected $base_dir;
    protected $processed_dir;
    protected $slack_client;

    public function __construct(SlackApiClient $slack_client)
    {
        $this->slack_client = $slack_client;
        $this->base_dir = base_path() . config('collections.sets_xml_path');
        $this->processed_dir = base_path() . config('collections.processed_xml_path');
    }


    public function parse()
    {
        $files = $this->getRawXmlFiles();

        if (count($files) == 0) {
            $this->sendMessage(self::EMPTY_FOLDER);

            return 'sets_xml folder is empty';
        }

        $app = new Container();
        $document = new Document($app);
        $reader = new Reader($document);

        foreach ($files as $file) {
            $this->processFile($reader, $file);
        }
    }

    protected function processFile($reader, $file_path)
    {
        $file = File::get($this->base_dir . $file_path);

        $xml = $reader->extract($file);

        $sets = $xml->parse([
            'name' => ['uses' => 'PromoActionName'],
            'start_date' => ['uses' => 'PromoActionStartDate'],
            'end_date' => ['uses' => 'PromoActionEndDate'],
            'discounts' => [
                'uses' => 'TableDiscounts.Discount[DiscountCondition,DiscountAmount,DiscountName,DiscountType,DiscountCode,DiscountSegment.DiscountSegmentItemNo(@=@)]'
            ],
            'conditions' => [
                'uses' => 'TableDiscounts.Discount[TableConditions.Condition.ConditionSegment.ConditionSegmentLine(ConditionSegmentNameItemNo=ConditionSegmentNameQty)>qty]'
            ]
        ]);

        $this->storeSets($sets);
        // TODO раскомментировать по окончанию экспериментов
        $moved = File::move($this->base_dir . $file_path, $this->processed_dir . time() . '.xml');
    }

    protected function getRawXmlFiles()
    {
        $files_list = array_diff(scandir($this->base_dir), array('..', '.'));

        return $files_list;
    }

    protected function storeSets($sets)
    {
        $created_set_theme = Sets::createSetTheme([
            'name' => $sets['name'],
            'slug' => Str::slug($sets['name'], '_'),
            'date_from' => $sets['start_date'],
            'date_to' => $sets['end_date'],
            'active' => 1,
        ]);

        $this->sendMessage(
            'Создана категория комплектов ' . $sets['name'] . ' с товарами'
            . PHP_EOL
            . 'Дата начала - ' . $sets['start_date']
            . PHP_EOL
            . 'Дата окончания - ' .  $sets['end_date']
        );

        foreach ($sets['discounts'] as $key => $set) {

            $created_set = Sets::createSet([
                'set_theme_id' => $created_set_theme->id,
                'set_id' => $set['DiscountCode']
            ]);

            $created_set_benefit = Sets::createSetBenefit($created_set, $set['DiscountAmount']);

            foreach ($set['DiscountSegment.DiscountSegmentItemNo'] as $barcode) {
                $quantity = $sets['conditions'][$key]['qty'][$barcode];

                $created_set_product = Sets::createSetProduct($created_set, $barcode, $quantity);
            }
        }
    }

    protected function sendMessage($message)
    {
        $this->slack_client->sendMessage([
            'text' => $message
        ]);
    }
}