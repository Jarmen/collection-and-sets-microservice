<?php

namespace lenal\collections\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Container\Container;
use Illuminate\Support\Facades\File;
use Orchestra\Parser\Xml\Document;
use Orchestra\Parser\Xml\Reader;
use lenal\collections\Facades\Collections;
use Illuminate\Support\Str;
use App\SlackApi\SlackApiClient;

class CollectionsParserController extends Controller
{
    const EMPTY_FOLDER = 'Скрипт парсера файлов коллекции завершил работу, но никаких изменений не произошло';
    const DUPLICATED_PRODUCT = 'При обработке файла коллекции обнаружены дубликаты товаров.';


    protected $base_dir;
    protected $processed_dir;
    protected $slack_client;

    public function __construct(SlackApiClient $slack_client)
    {
        $this->slack_client = $slack_client;
        $this->base_dir = base_path() . config('collections.raw_xml_path');
        $this->processed_dir = base_path() . config('collections.processed_xml_path');
    }


    public function parse()
    {
        $files = $this->getRawXmlFiles();

        if (count($files) == 0) {
            $this->sendMessage(self::EMPTY_FOLDER);

            return 'raw_xml folder is empty';
        }

        $app = new Container();
        $document = new Document($app);
        $reader = new Reader($document);

        foreach ($files as $file) {
            $this->processFile($reader, $file);
        }
    }

    protected function processFile($reader, $file_path)
    {
        $file = File::get($this->base_dir . $file_path);

        $xml = $reader->extract($file);

        $collection = $xml->parse([
            'name' => ['uses' => 'PromoActionName'],
            'start_date' => ['uses' => 'PromoActionStartDate'],
            'end_date' => ['uses' => 'PromoActionEndDate'],
            'discounts' => ['uses' => 'TableDiscounts.Discount[DiscountCondition,DiscountAmount,DiscountName,DiscountType]'],
            'products' => ['uses' => 'TableDiscounts.Discount.DiscountSegment[DiscountSegmentItemNo(@=@)]']
        ]);

        $this->storeCollection($collection);
        // TODO раскомментировать по окончанию экспериментов
        $moved = File::move($this->base_dir . $file_path, $this->processed_dir . time() . '.xml');



    }

    protected function getRawXmlFiles()
    {
        $files_list = array_diff(scandir($this->base_dir), array('..', '.'));

        return $files_list;
    }

    protected function storeCollection($collection)
    {
        $duplicate_products = null;

        $created_collection = Collections::createCollection([
            'collection_id' => Str::slug($collection['name'], '_'),
        ]);

        $created_products_collection = Collections::createProductCollection([
            'active' => 1,
            'name' => $collection['name'],
            'slug' => Str::slug($collection['name'], '_'),
            'date_from' => $collection['start_date'],
            'date_to' => $collection['end_date']
        ]);

        $this->sendMessage(
            'Создана коллекция ' . $collection['name'] . ' с товарами'
            . PHP_EOL
            . 'Дата начала - ' . $collection['start_date']
            . PHP_EOL
            . 'Дата окончания - ' .  $collection['end_date']
        );

        foreach ($collection['discounts'] as $collection_entity) {
            $created_benefit = Collections::createBenefit([
                'collection_id' => $created_collection->id,
                'name' => 'percent',
                'benefit_value' => $collection_entity['DiscountAmount']
            ]);

            $this->sendMessage(
                'Создана сущность Выгода '
                . 'Тип - '
                . $collection_entity['DiscountType']
                . ' Значение - '
                . $collection_entity['DiscountAmount']
            );

            $created_condition = Collections::createCondition([
                'name' => 'amount_in_basket',
                'condition_value' => $collection_entity['DiscountCondition'],
                'benefit_id' => $created_benefit->id,
                'collection_id' => $created_collection->id
            ]);

            $this->sendMessage(
                'Создана сущность Условие коллекции '
                . 'Тип - количество в корзине '
                . 'Значение - '
                . $collection_entity['DiscountCondition']
            );
        }

        foreach ($collection['products'][0]['DiscountSegmentItemNo'] as $index => $product_barcode) {

            if (Collections::findByBarcode($product_barcode)) {
                $duplcated_products[] = $product_barcode;
                continue;
            }

            Collections::createProduct([
                'collection_id' => $created_collection->id,
                'barcode' => $product_barcode
            ]);
        }

        if (! empty($duplcated_products)) {
            $this->sendMessage(self::DUPLICATED_PRODUCT . ' Артикулы: ' . implode(',', $duplcated_products));
        }
    }

    protected function sendMessage($message)
    {
        $this->slack_client->sendMessage([
            'text' => $message
        ]);
    }
}