<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_collections', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('active');
            $table->string('name');
            $table->string('slug');
            $table->string('preview_img')->nullable();
            $table->string('article_img')->nullable();
            $table->string('article_text')->nullable();
            $table->dateTime('date_from');
            $table->dateTime('date_to');
            $table->tinyInteger('uploaded')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_collections');
    }
}
