<?php

namespace lenal\collections\Helpers;

use lenal\collections\Models\Product;
use lenal\collections\Models\Condition;
use lenal\collections\Models\Benefit;
use lenal\collections\Models\SetBenefit;
use lenal\collections\Models\SetProduct;

class RecountCart {

    public function recount($cart)
    {
        $found_collections = $this->checkConditionsAndBenefits($cart);
        //$found_sets = $this->checkIfSetIsFull($cart);

        if (empty($found_collections)) {
            return ['status' => 'no changes'];
        }

        $recounted_cart = $this->recountCollections($cart, $found_collections);

//        if (! empty($found_sets)) {
//            $recounted_cart = $this->recountSets($recounted_cart, $found_sets);
//        }


        return $recounted_cart;
    }

    protected function checkConditionsAndBenefits($cart)
    {
        $found_collections = [];

        foreach($cart as $row_id => $item) {
            $barcodes[] = $item['options']['sku'];
            $barcode_quantity[$item['options']['sku']] = (int)$item['qty'];
        }

        $collection_products = Product::whereIn('barcode', $barcodes)->get();

        foreach ($collection_products as $product) {
            $found_collections[$product->collection_id]['barcodes'][] = $product->barcode;

            if (! empty($found_collections[$product->collection_id]['count'])) {
                $found_collections[$product->collection_id]['count'] += $barcode_quantity[$product->barcode];
            }

            if (empty($found_collections[$product->collection_id]['count'])) {
                $found_collections[$product->collection_id]['count'] = $barcode_quantity[$product->barcode];
            }

        }

        if (! empty($found_collections)) {
            foreach ($found_collections as $collection_id => $items) {
                $conditions = Benefit::where('benefits.collection_id', '=', $collection_id)
                    ->leftJoin('conditions', 'conditions.benefit_id', '=', 'benefits.id')
                    ->where('conditions.condition_value', '=', $items['count'])
                    ->where('conditions.name', '=', 'amount_in_basket')
                    ->first();

                $found_collections[$collection_id]['benefit_value'] = !empty($conditions->benefit_value)
                    ? $conditions->benefit_value
                    : 0;

                $max_condition = $this->getMaxCondition($collection_id);

                if ($items['count'] > $max_condition) {
                    $found_collections[$collection_id]['benefit_value'] = $this->getMaxBenefit($collection_id);
                }
            }
        }

        return $found_collections;
    }

    public function checkIfSetIsFull($cart)
    {
        $cart_sets = [];
        // получаем из корзины все товары, у которых есть set_id и группируем их
        foreach ($cart as $row_id => $item) {
            if ($item['options']['set_id'] > 0) {
                 $cart_sets[$item['options']['set_id']]['barcodes'][] = $item['options']['sku'];
            }
        }

        // обходим сгруппированый массив и и получаем из базы комплекты
        if (! empty($cart_sets)) {
            foreach ($cart_sets as $set_id => $barcodes) {
                $service_sets_obj = SetProduct::where('set_id', '=', $set_id)
                    ->get();

                $service_sets = $this->prepareBarcodes($service_sets_obj);

                $sets_diff = array_diff($service_sets, $barcodes['barcodes']);

                $cart_sets[$set_id]['benefit_value'] = SetBenefit::where('set_id', '=', $set_id)->first()->benefit_value;
                if (! empty($sets_diff)) {
                    unset($cart_sets[$set_id]);
                }

            }
        }

        return $cart_sets;
    }

    protected function recountSets($cart, $found_sets)
    {
        foreach ($found_sets as $id => $set) {
            foreach ($cart as $row_id => $item) {
                if(in_array($item['options']['sku'], $found_sets[$id]['barcodes'])) {

                    $discount_value = $this->calculateDiscountValue(
                        $item['price'],
                        $found_sets[$id]['benefit_value']);

                    $cart[$row_id]['options']['old_price'] = $item['price'];
                    $cart[$row_id]['price'] = $item['price'] - $discount_value;
                    $cart[$row_id]['options']['discount_value'] = $found_sets[$id]['benefit_value'];
                }
            }
        }

        return $cart;
    }

    protected function recountCollections($cart, $found_collections)
    {
        foreach ($found_collections as $id => $collection) {
            foreach ($cart as $row_id => $item) {
                if(in_array($item['options']['sku'], $found_collections[$id]['barcodes'])) {

                    $discount_value = $this->calculateDiscountValue(
                        $item['price'],
                        $found_collections[$id]['benefit_value']);

                    $cart[$row_id]['options']['old_price'] = $item['price'];
                    $cart[$row_id]['price'] = $item['price'] - $discount_value;
                    $cart[$row_id]['options']['discount_value'] = $found_collections[$id]['benefit_value'];
                }
            }
        }

        return $cart;
    }

    protected function calculateDiscountValue($price, $benefit_value)
    {
        $calculated = floor(($price * $benefit_value)/100);

        return $calculated;
    }

    protected function getMaxCondition($collection_id)
    {
        return Condition::where('collection_id', $collection_id)->max('condition_value');
    }

    protected function getMaxBenefit($collection_id)
    {
        return Benefit::where('collection_id', $collection_id)->max('benefit_value');
    }

    protected function prepareBarcodes($set)
    {
        foreach ($set as $barcode) {
            $barcodes[] = $barcode->barcode;
        }

        return $barcodes;
    }
}