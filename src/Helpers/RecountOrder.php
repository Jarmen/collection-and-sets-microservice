<?php

namespace lenal\collections\Helpers;

use lenal\collections\Models\Collection;
use lenal\collections\Models\Product;
use lenal\collections\Models\Condition;
use lenal\collections\Models\Benefit;
use lenal\collections\Models\SetBenefit;

class RecountOrder {

    public function recount($order)
    {
        $found_collections = $this->checkConditionsAndBenefits($order);
        $found_sets = $this->checkOrderForSets($order);

        if (count($found_collections) == 0 && count($found_sets) == 0) {
            return ['status' => 'no changes'];
        }

        $recounted_order = $this->recountOrder($order, $found_collections);
        $recounted_order = $this->recountOrderWithSets($recounted_order);

        $final_order = $this->recountOrderSum($recounted_order);

        return $final_order;
    }

    public function checkConditionsAndBenefits($order)
    {
        $found_collections = [];

        foreach($order['productArray'] as $id => $item) {
            $barcodes[] = $item['code1c'];
            $barcode_quantity[$item['code1c']] = (int)$item['qty'];
        }

        $collection_products = Product::whereIn('barcode', $barcodes)
            ->leftJoin('product_collections', 'product_collections.id', '=', 'products.collection_id')
            ->get();

        foreach ($collection_products as $product) {
            $found_collections[$product->collection_id]['barcodes'][] = $product->barcode;
            $found_collections[$product->collection_id]['collection_name'] = $product->name;

            if (! empty($found_collections[$product->collection_id]['count'])) {
                $found_collections[$product->collection_id]['count'] += $barcode_quantity[$product->barcode];;
            }

            if (empty($found_collections[$product->collection_id]['count'])) {
                $found_collections[$product->collection_id]['count'] = $barcode_quantity[$product->barcode];;
            }

        }

        if (count($found_collections) > 0) {
            foreach ($found_collections as $collection_id => $items) {
                $conditions = Benefit::where('benefits.collection_id', '=', $collection_id)
                    ->leftJoin('conditions', 'conditions.benefit_id', '=', 'benefits.id')
                    ->where('conditions.condition_value', '=', $items['count'])
                    ->where('conditions.name', '=', 'amount_in_basket')
                    ->first();

                $found_collections[$collection_id]['benefit_value'] = !empty($conditions->benefit_value)
                    ? $conditions->benefit_value
                    : 0;

                $max_condition = $this->getMaxCondition($collection_id);

                if ($items['count'] > $max_condition) {
                    $found_collections[$collection_id]['benefit_value'] = $this->getMaxBenefit($collection_id);
                }
            }
        }

        return $found_collections;
    }

    protected function recountOrder($order, $found_collections)
    {
        foreach ($found_collections as $id => $collection) {
            foreach ($order['productArray'] as $item_id => $item) {
                if(in_array($item['code1c'], $found_collections[$id]['barcodes'])) {

                    $discount_value = $this->calculateDiscountValue(
                        $item['price'],
                        $found_collections[$id]['benefit_value']);

                    $order['productArray'][$item_id]['collection'] = $found_collections[$id]['collection_name'];
                    $order['productArray'][$item_id]['recounted_price'] = ($item['price'] - $discount_value) * $item['qty'];
                    $order['productArray'][$item_id]['discount_value'] = $discount_value;
                }
            }
        }

        return $order;
    }

    protected function calculateDiscountValue($price, $benefit_value)
    {
        $calculated = floor(($price * $benefit_value)/100);

        return $calculated;
    }

    protected function getMaxCondition($collection_id)
    {
        return Condition::where('collection_id', $collection_id)->max('condition_value');
    }

    protected function getMaxBenefit($collection_id)
    {
        return Benefit::where('collection_id', $collection_id)->max('benefit_value');
    }

    protected function recountOrderSum($order)
    {
        $order_sum_with_discount = 0;
        $discount_sum = 0;

        foreach ($order['productArray'] as $item) {
            $order_sum_with_discount += isset($item['recounted_price'])
                ? $item['recounted_price']
                : $item['price'] * $item['qty'];

            $discount_sum += isset($item['discount_value'])
                ? $item['discount_value']
                : 0;
        }

        $order['total_with_discount'] = $order_sum_with_discount;
        $order['total_discount'] = $discount_sum;

        return $order;
    }

    protected function recountOrderWithSets($order)
    {
        foreach ($order['productArray'] as $item_id => $item) {
            if (empty($item['set_id'])) {
                continue;
            }

            $set_benefit = SetBenefit::where('set_id', '=', $item['set_id'])->first();

            if (empty($set_benefit->benefit_value)) {
                continue;
            }

            $discount_value = $this->calculateDiscountValue($item['price'], $set_benefit->benefit_value);
            $order['productArray'][$item_id]['recounted_price'] = ($item['price'] - $discount_value) * $item['qty'];
            $order['productArray'][$item_id]['discount_value'] = $discount_value * $item['qty'];
        }

        return $order;
    }

    protected function checkOrderForSets($order)
    {
        $found_sets = [];

        foreach ($order['productArray'] as $item) {
            if (isset($item['set_id']) && ! empty($item['set_id'])) {
                $found_sets[$item['set_id']] = $item['set_id'];
            }
        }

        return $found_sets;
    }
}