<?php

namespace lenal\collections\Helpers;

use App\SlackApi\SlackApiClient;
use Carbon\Carbon;
use lenal\collections\Models\Collection;
use lenal\collections\Models\Product;
use lenal\collections\Models\Condition;
use lenal\collections\Models\ProductCollection;
use lenal\collections\Models\Benefit;

class Collections {

    protected $slack_client;

    public function __construct(SlackApiClient $slack_client)
    {
        $this->slack_client = $slack_client;
    }

    public function createCollection($collection)
    {
        $created = Collection::firstOrCreate($collection);

        return $created;
    }

    public function createBenefit($benefit)
    {
        $created = Benefit::firstOrCreate($benefit);

        return $created;
    }

    public function createCondition($condition)
    {
        $created = Condition::firstOrCreate($condition);

        return $created;
    }

    public function createProduct($product)
    {
        $created = Product::firstOrCreate($product);

        return $created;
    }

    public function getActiveCollections()
    {
        $collections = ProductCollection::where('active', '>', 0)
            ->where('uploaded', '=', 0)
            ->leftJoin('products', 'products.collection_id', '=', 'product_collections.id')
            ->get();

        $collections_ids = $collections->pluck('id');

        $this->markAsUploaded($collections_ids);

        return $collections->toJson();
    }

    public function createProductCollection($collection)
    {
        $created = ProductCollection::firstOrCreate($collection);

        return $created;
    }

    public function getAllCollections()
    {
        return ProductCollection::all();
    }

    public function checkDateAndDisable($collection)
    {
        if ($collection->date_to < Carbon::today()) {

            $deleted_collection = ProductCollection::where('id', $collection->id)
                ->first();

            ProductCollection::where('id', $collection->id)
                ->delete();

            Collection::where('id', $collection->id)
                ->delete();

            Condition::where('collection_id', $collection->id)
                ->delete();

            Benefit::where('collection_id', $collection->id)
                ->delete();

            Product::where('collection_id', $collection->id)
                ->delete();

            $this->sendMessage(
                'Срок действия коллекции '
                . $deleted_collection->name
                . ' истек. Она была удалена из системы для избежания ошибок пересчета'
            );
        }
    }

    public function findByBarcode($barcode)
    {
        if (Product::where('barcode', $barcode)->first()) {
            return true;
        }

        return false;
    }

    protected function sendMessage($message)
    {
        $this->slack_client->sendMessage([
            'text' => $message
        ]);
    }

    protected function markAsUploaded($ids)
    {
        $updated = ProductCollection::whereIn('id', $ids)
            ->update([
                'uploaded' => 1
            ]);
    }
}