<?php

namespace lenal\collections\Helpers;

use App\SlackApi\SlackApiClient;
use Carbon\Carbon;
use lenal\collections\Models\Set;
use lenal\collections\Models\SetTheme;
use lenal\collections\Models\SetBenefit;
use lenal\collections\Models\SetProduct;

class Sets {

    protected $slack_client;

    public function __construct(SlackApiClient $slack_client)
    {
        $this->slack_client = $slack_client;
    }

    public function createSetTheme($set_theme)
    {
        $created = SetTheme::firstOrCreate($set_theme);

        return $created;
    }

    public function createSetBenefit($set, $amount)
    {
        $created = SetBenefit::firstOrCreate([
            'name' => 'percent',
            'benefit_value' => $amount,
            'set_id' => $set->set_id
        ]);

        return $created;
    }

    public function createSet($set)
    {
        $created = Set::create($set);

        return $created;
    }

    public function createSetProduct($set, $barcode, $quantity)
    {
        $created = SetProduct::firstOrCreate([
            'set_id' => $set->set_id,
            'barcode' => $barcode,
            'quantity' => $quantity
        ]);

        return $created;
    }

    public function getActiveSets()
    {
        $sets = SetTheme::where('active', '>', 0)
            ->where('uploaded', '=', 0)
            ->leftJoin('sets', 'sets.set_theme_id', '=', 'sets_themes.id')
            ->leftJoin('sets_products', 'sets_products.set_id', '=', 'sets.set_id')
            ->leftJoin('sets_benefits', 'sets_benefits.set_id', '=', 'sets.set_id')
            ->get([
                'sets_themes.slug as set_theme_id',
                'sets_products.barcode',
                'sets.set_id as set_id',
                'sets_benefits.benefit_value',
                'sets_products.quantity'
            ]);

        $sets_themes = SetTheme::where('active', '>', 0)
            ->where('uploaded', '=', 0)
            ->get();

        $sets_ids = $sets_themes->pluck('id');

        $this->markAsUploaded($sets_ids);

        return $sets->toJson();
    }

    public function getActiveSetThemes()
    {
        $set_themes = SetTheme::where('active', '>', 0)
            ->where('uploaded', '=', 0)
            ->get(['name', 'slug', 'date_from', 'date_to']);

        return $set_themes->toJson();
    }

    public function checkDateAndDisable($set_theme)
    {
        if ($set_theme->date_to < Carbon::today()) {

            $deleted_set_theme = SetTheme::where('id', '=', $set_theme->id)->first();

            $sets = Set::where('set_theme_id', '=', $set_theme->id)->pluck('set_id');

            SetTheme::where('id', '=', $set_theme->id)
                ->delete();

            Set::whereIn('set_id', $sets)
                ->delete();

            SetProduct::whereIn('set_id', $sets)
                ->delete();

            SetBenefit::whereIn('set_id', $sets)
                ->delete();

            $this->sendMessage(
                'Срок действия категории комлектов '
                . $deleted_set_theme->name
                . ' истек. Она была удалена из системы для избежания ошибок пересчета'
            );
        }
    }

    protected function sendMessage($message)
    {
        $this->slack_client->sendMessage([
            'text' => $message
        ]);
    }

    protected function markAsUploaded($ids)
    {
        $updated = SetTheme::whereIn('id', $ids)
            ->update([
                'uploaded' => 1
            ]);
    }
}