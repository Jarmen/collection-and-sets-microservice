<?php

// сюда приходит корзина заказ со стороны сайта
Route::post('/recount_order_from_site', 'lenal\collections\Controllers\SiteOrdersController@recount');

// сюда приходит корзина заказа со стороны crm onebox
Route::post('/recount_order_from_crm', 'lenal\collections\Controllers\OneBoxOrdersController@recount');
Route::get('/recount_order_from_crm', 'lenal\collections\Controllers\OneBoxOrdersController@recount');

Route::get('/secret/parse', 'lenal\collections\Controllers\CollectionsParserController@parse');
Route::get('/secret/sets_parse', 'lenal\collections\Controllers\SetsParserController@parse');
Route::get('/secret/collections', 'lenal\collections\Controllers\CollectionsController@index');
Route::get('/secret/set_themes', 'lenal\collections\Controllers\SetsController@indexSetThemes');
Route::get('/secret/sets', 'lenal\collections\Controllers\SetsController@index');
Route::get('/secret/disable_outdated', 'lenal\collections\Controllers\CollectionsController@disable');
Route::get('/secret/disable_outdated_sets', 'lenal\collections\Controllers\SetsController@disable');

Route::post('/secret/json/parse', 'lenal\collections\Controllers\JsonCollectionsParserController@parse');
