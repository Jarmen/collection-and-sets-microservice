<?php

return [
    'raw_xml_path' => '/raw_xml/',
    'sets_xml_path' => '/sets_xml/',
    'processed_xml_path' => '/processed_xml/'
];