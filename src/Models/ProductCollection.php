<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCollection extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'active',
        'name',
        'slug',
        'preview_img',
        'article_img',
        'article_text',
        'date_from',
        'date_to',
        'uploaded'
    ];
}
