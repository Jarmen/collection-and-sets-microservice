<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'barcode',
        'collection_id'
    ];
}
