<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'condition_id',
        'benefit_value',
        'collection_id'
    ];

    public function conditions()
    {
        return $this->hasMany('lenal\collections\Models\Condition');
    }
}
