<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class SetTheme extends Model
{
    public $timestamps = false;

    public $table = 'sets_themes';

    protected $fillable = [
        'name', 'date_from', 'date_to', 'slug', 'active', 'uploaded'
    ];
}
