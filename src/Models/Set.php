<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'set_theme_id', 'barcode', 'set_id'
    ];
}
