<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'collection_id'
    ];
}
