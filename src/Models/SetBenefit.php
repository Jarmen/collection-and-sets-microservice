<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class SetBenefit extends Model
{
    public $timestamps = false;

    public $table = 'sets_benefits';

    protected $fillable = [
        'name', 'benefit_value', 'set_id'
    ];
}
