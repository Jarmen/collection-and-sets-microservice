<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class SetProduct extends Model
{
    public $timestamps = false;

    protected $table = 'sets_products';

    protected $fillable = [
        'set_id', 'barcode', 'quantity'
    ];

    public function scopeProducts($query, $set_id)
    {
        return $query->where('set_id', '=', $set_id);
    }
}
