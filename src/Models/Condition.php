<?php

namespace lenal\collections\Models;

use Illuminate\Database\Eloquent\Model;

class Condition extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'benefit_id',
        'condition_value',
        'collection_id'
    ];
}
