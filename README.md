## lenal/collections  
  
Sales and products sets, prices recounting and api for crm integration
  
### Install  
  
  * Add to composer.json  
  
  ```  
  "repositories": [  
          {  
              "type": "git",  
              "url":  "git@gitlab.com:Jarmen/collection-and-sets-microservices.git"  
          }  
      ]  
  ```   
  
  If prompted, insert login/password or create auth.json in your global composer directory
    
  * Run command  
  
  ```
  composer require lenal/collections  
  ```  
  
  ### Basic usage  
  * Run command  
  
  ```
  php artisan vendor:publish --provider=lenal\collections\CollectionsServiceProvider --tag=config
  ```  
  
  to copy configuration file into project
      
  * Run command 
  
  ```
    php artisan migrate
   ```  
    
  ### Package customization  
  
  * Run command  
  
  ```
    php artisan vendor:publish  
  ```  
  
   select package from list
  
  All files wiil be copied to packages/lenal/collections   
  
  * Register package namespace in composer.json  
  
  ```
    #!json
    "autoload": {
            "psr-4": {
                "lenal\\collections\\": "packages/lenal/collections/src"
            }
        },
   ```   
   
   * Add service provider to 'providers' section in  config/app.php  
   
   ```  
   /*  
    * Package Service Providers...  
    */  
    lenal\collections\CollectionsServiceProvider::class,  
   ```  
   
   * Run command  
   
   ```
   composer dumpautoload
   ```
   
   To prevent namespace collision remove package from vendor
   
   ```
   composer remove lenal/collections
   ```  

  